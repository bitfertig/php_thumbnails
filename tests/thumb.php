<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once '../thumb.class.php';

function build($conf) {

  $filename = dirname(__FILE__).'/example.jpg';

  $t = new ThumbImage($filename);

  $width = (isset($conf['width'])) ? $conf['width'] : 'auto';
  $height = (isset($conf['height'])) ? $conf['height'] : 'auto';
  $t->setCanvas($width, $height);

  if (isset($conf['size'])) {
    $t->setSize($conf['size']);
  }
  if (isset($conf['position'])) {
    $t->setPosition($conf['position']);
  }
  if (isset($conf['trimcanvas'])) {
    $t->setTrimCanvas($conf['trimcanvas']);
  }
  if (isset($conf['quality'])) {
    $t->setQuality($conf['quality']);
  }

  $t->resize();

  $dir = pathinfo($filename, PATHINFO_DIRNAME).'/thumbs';
  $fil = pathinfo($filename, PATHINFO_FILENAME);
  $ext = pathinfo($filename, PATHINFO_EXTENSION);
  $newfilestr = $fil.$t->getThumbStr().'.'.$ext;

  $t->save($dir.'/'.$newfilestr, IMAGETYPE_JPEG, null);

  return 'thumbs/'.$newfilestr;

}
?>
<!DOCTYPE html>
<html lang="de" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Thumb Class</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <h1>Thumb Class</h1>

  We use this <a href="example.jpg">original image</a> as example to create thumbnails on the fly.

  <?php
  $tests = array(
    array('name'=>'Contain', 'css'=>'background-size:contain;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'contain']),
    array('name'=>'Cover', 'css'=>'background-size:cover;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'cover']),
    array('name'=>'auto (auto auto)', 'css'=>'background-size:auto;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'auto']),
    array('name'=>'auto 50%', 'css'=>'background-size:auto 50%;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'auto 50pct']),
    array('name'=>'50% auto', 'css'=>'background-size:50% auto;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'50pct auto']),
    array('name'=>'50% 50%', 'css'=>'background-size:50% 50%;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'50pct 50pct']),
    array('name'=>'50px (50px auto)', 'css'=>'background-size:50px auto;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'50px']),
    array('name'=>'auto 50px', 'css'=>'background-size:auto 50px;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'auto 50px']),
    array('name'=>'50px 50px', 'css'=>'background-size:50px 50px;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'50px 50px']),
    array('name'=>'Cover und (position: 50% 50%)', 'css'=>'background-size:cover; background-position:50% 50%;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'cover', 'position'=>'50pct 50pct']),
    array('name'=>'auto (auto auto) und (position: 50% 50%)', 'css'=>'background-size:auto; background-position:50% 50%;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'auto', 'position'=>'50pct 50pct']),
    array('name'=>'(50px auto) und (position: 50% 50%) und (trimcanvas: false)', 'css'=>'background-size:50px auto;background-position:50% 50%;', 'conf' => ['width'=>200, 'height'=>200, 'size'=>'50px auto', 'position'=>'50pct 50pct', 'trimcanvas'=>false])
  );

  foreach ($tests as $test) {
    $filename = build($test['conf']);
    echo '<h3>'. $test['name']. '</h3>';
    echo '<table>';
    echo '<tr><th>CSS</th><th>Thumb</th></tr>';
    echo '<tr>';
    echo '<td class="code"><pre>'. $test['css']. '</pre></td>';
    echo '<td class="code"><pre>'. print_r($test['conf'], true) .'</pre></td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td><div class="img cssimg" style="background-image:url(./example.jpg);'. $test['css'] .'"></div></td>';
    echo '<td><div class="img thumbimg"><img src="'. $filename .'"></div></td>';
    echo '</tr>';
    echo '</table>';
    echo '<hr>';
  }
  ?>


</body>
</html>
